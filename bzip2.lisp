;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; bzip2.lisp

;;; Copyright (c) 2008, Chaitanya Gupta.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 1. Redistributions of source code must retain the above copyright
;;;    notice, this list of conditions and the following disclaimer.
;;; 2. Redistributions in binary form must reproduce the above copyright
;;;    notice, this list of conditions and the following disclaimer in the
;;;    documentation and/or other materials provided with the distribution.
;;; 3. The name of the author may not be used to endorse or promote products
;;;    derived from this software without specific prior written permission.
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
;;; IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
;;; OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
;;; INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
;;; NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
;;; THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(defpackage #:cl-bzip2
  (:use #:cl #:cffi)
  (:nicknames #:bzip2)
  (:export "COMPRESS"
           "DECOMPRESS"
           "BZ-ERROR"))

(in-package #:cl-bzip2)

;;; Define and load libbzip2

(define-foreign-library libbz2
  (:darwin "libbz2.dylib")
  (:unix "libbz2.so")
  (t (:default "libbz2")))

(with-simple-restart (skip "Skip loading foreign library LIBBZIP2.")
  (use-foreign-library libbz2))

;;; bz return codes

(defvar *bz-return-codes* (make-hash-table))

(defmacro define-bz-return-code (name code)
  (let ((var-name (format nil "+~A+" (substitute #\- #\_ (string name)))))
    `(progn
       (setf (gethash ,code *bz-return-codes*) ',name)
       (defconstant ,(intern var-name :bzip2) ,code))))

(define-bz-return-code BZ_OK 0)
(define-bz-return-code BZ_RUN_OK 1)
(define-bz-return-code BZ_FLUSH_OK 2)
(define-bz-return-code BZ_FINISH_OK 3)
(define-bz-return-code BZ_STREAM_END 4)
(define-bz-return-code BZ_SEQUENCE_ERROR -1)
(define-bz-return-code BZ_PARAM_ERROR -2)
(define-bz-return-code BZ_MEM_ERROR -3)
(define-bz-return-code BZ_DATA_ERROR -4)
(define-bz-return-code BZ_DATA_ERROR_MAGIC -5)
(define-bz-return-code BZ_IO_ERROR -6)
(define-bz-return-code BZ_UNEXPECTED_EOF -7)
(define-bz-return-code BZ_OUTBUFF_FULL -8)
(define-bz-return-code BZ_CONFIG_ERROR -9)

;;; bz_stream

(defcstruct bz-stream
  (next-in :pointer)
  (avail-in :uint)
  (total-in-lo32 :uint)
  (total-in-hi32 :uint)
  
  (next-out :pointer)
  (avail-out :uint)
  (total-out-lo32 :uint)
  (total-out-hi32 :uint)
  
  (state :pointer)
  
  (bzalloc :pointer)
  (bzfree :pointer)
  (opaque :pointer))

(defun bz-stream-slot-value (object slot-name)
  (foreign-slot-value object 'bz-stream slot-name))

(defun (setf bz-stream-slot-value) (value object slot-name)
  (setf (foreign-slot-value object 'bz-stream slot-name) value))

(defun make-bz-stream ()
  (let ((bz-stream (foreign-alloc 'bz-stream)))
    (setf (bz-stream-slot-value bz-stream 'bzalloc) (null-pointer)
          (bz-stream-slot-value bz-stream 'bzfree) (null-pointer)
          (bz-stream-slot-value bz-stream 'opaque) (null-pointer))
    bz-stream))

(defun free-bz-stream (bz-stream)
  (foreign-free bz-stream))

(defmacro with-bz-stream ((var) &body body)
  `(let ((,var (make-bz-stream)))
     (unwind-protect (progn ,@body)
       (free-bz-stream ,var))))

;;; Error handling

(define-condition bz-error (error)
  ((code :initarg :code
         :reader bz-error-code
         :initform (error "CODE is required."))
   (cfun :initarg :cfun
         :reader bz-error-cfun
         :initform (error "CFUN is required.")))
  (:report (lambda (c stream)
             (format stream "Error in function ~A: ~A (~A)"
                     (bz-error-cfun c)
                     (gethash (bz-error-code c) *bz-return-codes*)
                     (bz-error-code c))))
  (:documentation "The default condition type for any BZIP2
  compression/decompression related error."))

(defun bz-error (code cfun)
  (error 'bz-error
         :code code
         :cfun cfun))

(defun ensure-list (item)
  (etypecase item
    (atom (list item))
    (list item)))

(defmacro with-bz-error-handling ((cfun &key accept deny) &body body)
  (assert (not (and accept deny))
          ()
          "ACCEPT and DENY cannot both be non-NIL.")
  (let ((return-code (gensym "RET-CODE")))
    `(let ((,return-code (progn
                           ,@body)))
       ,(cond
         (accept `(unless (member ,return-code (ensure-list ,accept))
                    (bz-error ,return-code ,cfun)))
         (deny `(when (member ,return-code (ensure-list ,deny))
                  (bz-error ,return-code ,cfun))))
       ,return-code)))

;;; Wrappers over low-level compress API

(defcfun ("BZ2_bzCompressInit" %bz-compress-init) :int
  (bz-stream :pointer)
  (block-size-100k :int)
  (verbosity :int)
  (work-factor :int))

(defun bz-compress-init (bz-stream block-size-100k verbosity work-factor)
  (with-bz-error-handling ("BZ2_bzCompressInit" :accept +bz-ok+)
    (%bz-compress-init bz-stream block-size-100k verbosity work-factor)))

;;;; Actions for BZ2_bzCompress

(defconstant +bz-run+ 0)
(defconstant +bz-flush+ 1)
(defconstant +bz-finish+ 2)

(defcfun ("BZ2_bzCompress" %bz-compress) :int
  (bz-stream :pointer)
  (action :int))

(defun bz-compress (bz-stream action)
  (flet ((!accept (code-designator)
           (with-bz-error-handling ("BZ2_bzCompress" :accept code-designator)
             (%bz-compress bz-stream action))))
    (cond 
      ((= action +bz-run+)
       (!accept +bz-run-ok+))
      ((= action +bz-flush+)
       (!accept +bz-flush-ok+))
      ((= action +bz-finish+)
       (!accept (list +bz-finish-ok+ +bz-stream-end+))))))

(defcfun ("BZ2_bzCompressEnd" %bz-compress-end) :int
  (bz-stream :pointer))

(defun bz-compress-end (bz-stream)
  (with-bz-error-handling ("BZ2_bzCompressEnd" :accept +bz-ok+)
    (%bz-compress-end bz-stream)))

;;; Wrappers over low-level decompress API

(defcfun ("BZ2_bzDecompressInit" %bz-decompress-init) :int
  (bz-stream :pointer)
  (verbosity :int)
  (small :int))

(defun bz-decompress-init (bz-stream verbosity smallp)
  (with-bz-error-handling ("BZ2_bzDecompressInit" :accept +bz-ok+)
    (%bz-decompress-init bz-stream
                         verbosity
                         (if smallp 1 0))))

(defcfun ("BZ2_bzDecompress" %bz-decompress) :int
  (bz-stream :pointer))

(defun bz-decompress (bz-stream)
  (with-bz-error-handling ("BZ2_bzDecompress" :accept (list +bz-ok+ +bz-stream-end+))
    (%bz-decompress bz-stream)))

(defcfun ("BZ2_bzDecompressEnd" %bz-decompress-end) :int
  (bz-strem :pointer))

(defun bz-decompress-end (bz-stream)
  (with-bz-error-handling ("BZ2_bzDecompressEnd" :accept +bz-ok+)
    (%bz-decompress-end bz-stream)))

;;;; Easy types

(deftype octet ()
  '(unsigned-byte 8))

(deftype octet-vector ()
  '(vector (unsigned-byte 8)))

;;; Block read/write from C memory

(defun mem-read-stream (stream ptr length)
  (loop
     for i from 0 to (1- length)
     do (write-byte (mem-aref ptr :uchar i) stream)))

(defun mem-write-vector (vector ptr &optional (count (length vector)))
  (loop
     for i below count
     do (setf (mem-aref ptr :uchar i) (aref vector i))))

;;; High level stuff

(defparameter *input-chunk-size* 4096)
(defparameter *output-chunk-size* 4096)

(defstruct (array-container
             (:constructor make-array-container-aux)
             (:conc-name nil))
  in-vec
  in-cvec
  out-cvec
  input-chunk-size
  output-chunk-size)

(defun make-array-container (input-chunk-size output-chunk-size)
  (make-array-container-aux
   :in-vec (make-sequence 'octet-vector input-chunk-size)
   :in-cvec (foreign-alloc :uchar :count input-chunk-size)
   :out-cvec (foreign-alloc :uchar :count output-chunk-size)
   :input-chunk-size input-chunk-size
   :output-chunk-size output-chunk-size))

(defun free-array-container (container)
  (foreign-free (in-cvec container))
  (foreign-free (out-cvec container)))

(defmacro with-array-container ((var &key
                                     (input-chunk-size '*input-chunk-size*)
                                     (output-chunk-size '*output-chunk-size*))
                                &body body)
  `(let ((,var (make-array-container ,input-chunk-size ,output-chunk-size)))
     (unwind-protect (progn ,@body)
       (free-array-container ,var))))

;;;; High level compression calls

(defun compress-stream-aux (bz-stream in-stream out-stream ac)
  (let* ((input-chunk-size (input-chunk-size ac))
         (output-chunk-size (output-chunk-size ac))
         (in-vec (in-vec ac))
         (in-size (read-sequence in-vec in-stream :start 0 :end input-chunk-size))
         (in-cvec (in-cvec ac))
         (out-cvec (out-cvec ac))
         (input-left-p (= in-size input-chunk-size)))
    (mem-write-vector in-vec in-cvec in-size)
    (with-foreign-slots ((next-in avail-in next-out avail-out) bz-stream bz-stream)
      (setf next-in in-cvec
            avail-in in-size)
      ;; The following code is a translation from the C source of
      ;; BZ2_bzWrite and BZ2_bzWriteClose64 in bzlib.c.
      ;; Using action BZ_RUN
      (block run
        (loop
           (setf next-out out-cvec
                 avail-out output-chunk-size)
           ;; bzCompress/BZ_RUN
           (bz-compress bz-stream +bz-run+)
           (when (< avail-out output-chunk-size)
             ;; Writing to output stream
             (mem-read-stream out-stream out-cvec (- output-chunk-size avail-out)))
           (when (= avail-in 0)
             (return-from run nil))))
      ;; Using action BZ_FINISH, only if no more input is left
      (unless input-left-p
        (block finish
          (loop
             (setf next-out out-cvec
                   avail-out output-chunk-size)
             (let ((return-code
                    ;; bzCompress/BZ_FINISH
                    (bz-compress bz-stream +bz-finish+)))
               (when (< avail-out output-chunk-size)
                 ;; Writing to output stream
                 (mem-read-stream out-stream out-cvec (- output-chunk-size avail-out)))
               (when (= return-code +bz-stream-end+)
                 (return-from finish nil)))))))
    (force-output out-stream)
    ;; More input available?
    input-left-p))

(defparameter *block-size-100k* 9)
(defparameter *verbosity* 0)
(defparameter *work-factor* 30)

(defun compress-stream (in-stream out-stream &key
                        (block-size-100k *block-size-100k*)
                        (verbosity *verbosity*)
                        (work-factor *work-factor*))
  (with-bz-stream (bz-stream)
    (bz-compress-init bz-stream block-size-100k verbosity work-factor)
    (unwind-protect
         (with-array-container (ac)
           (loop
              while (compress-stream-aux bz-stream in-stream out-stream ac)))
      (bz-compress-end bz-stream)))
  (values))

(defun compress (in out &key
                 (block-size-100k *block-size-100k*)
                 (verbosity *verbosity*)
                 (work-factor *work-factor*))
  "Compresses data from IN to OUT. IN or OUT can either be a
binary stream or a pathname. This function doesn't return any
value.

BLOCK-SIZE-100K (default 9), VERBOSITY (default 0) and
WORK-FACTOR (default 30) correspond to the parameters
`blockSize100k', `verbosity' and `workFactor', respectively, for
the libbzip2 function `BZ2_bzCompressInit'.

From the bzip2 manual:

Parameter `blockSize100k' specifies the block size to be used for
compression. It should be a value between 1 and 9 inclusive, and
the actual block size used is 100000 x this figure. 9 gives the
best compression but takes most memory.

Parameter `verbosity' should be set to a number between 0 and 4
inclusive. 0 is silent, and greater numbers give increasingly
verbose monitoring/debugging output.

Parameter `workFactor' controls how the compression phase behaves
when presented with worst case, highly repetitive, input data. If
compression runs into difficulties caused by repetitive data, the
library switches from the standard sorting algorithm to a
fallback algorithm. The fallback is slower than the standard
algorithm by perhaps a factor of three, but always behaves
reasonably, no matter how bad the input."
  (let ((*block-size-100k* block-size-100k)
        (*verbosity* verbosity)
        (*work-factor* work-factor))
    (compress-aux in out)))

(defgeneric compress-aux (in out)
  (:documentation "Specialize behaviour for COMPRESS depending on IN and OUT."))

(defmethod compress-aux ((in stream) (out stream))
  (compress-stream in out))

(defmethod compress-aux ((in pathname) out)
  (with-open-file (in-stream in :direction :input :element-type 'octet)
    (compress-aux in-stream out)))

(defmethod compress-aux (in (out pathname))
  (with-open-file (out-stream out :direction :output :element-type 'octet :if-exists :supersede)
    (compress-aux in out-stream)))

;;;; High level decompression calls

(defun decompress-stream-aux (bz-stream in-stream out-stream ac)
  (let* ((input-chunk-size (input-chunk-size ac))
         (output-chunk-size (output-chunk-size ac))
         (in-vec (in-vec ac))
         (in-size (read-sequence in-vec in-stream :start 0 :end input-chunk-size))
         (in-cvec (in-cvec ac))
         (out-cvec (out-cvec ac))
         (input-left-p (= in-size input-chunk-size)))
    (mem-write-vector in-vec in-cvec in-size)
    (with-foreign-slots ((next-in avail-in next-out avail-out) bz-stream bz-stream)
      (setf next-in in-cvec
            avail-in in-size)
      ;; bz_decompress
      (block decompress
        (loop
           (setf next-out out-cvec
                 avail-out output-chunk-size)
           (let ((return-code (bz-decompress bz-stream)))
             (when (< avail-out output-chunk-size)
               (mem-read-stream out-stream out-cvec (- output-chunk-size avail-out)))
             (when (or (= avail-in 0)
                       (= return-code +bz-stream-end+))
               (return-from decompress nil)))))
      ;; More input available?
      input-left-p)))

(defparameter *smallp* nil)

(defun decompress-stream (in-stream out-stream &key
                          (verbosity *verbosity*)
                          (smallp *smallp*))
  (with-bz-stream (bz-stream)
    (bz-decompress-init bz-stream verbosity smallp)
    (unwind-protect
         (with-array-container (ac)
           (loop
              while (decompress-stream-aux bz-stream in-stream out-stream ac)))
      (bz-decompress-end bz-stream)))
  (values))

(defun decompress (in out &key
                   (verbosity *verbosity*)
                   (smallp *smallp*))
  "Decompresses data from IN to OUT. IN or OUT can either be a
binary stream or a pathname. This function doesn't return any
value.

VERBOSITY and SMALLP (default NIL) correspond to the parameters
`verbosity' and `small', respectively, for the libbzip2 function
`BZ2_bzDecompressInit'.

For the meaning of VERBOSITY, see the documentation for
COMPRESS. A non-NIL value for SMALLP corresponds to a non-zero
value for the parameter `small'. Here's what the bzip2 manual
says about `small':

If `small' is nonzero, the library will use an alternative
decompression algorithm which uses less memory but at the cost of
decompressing more slowly (roughly speaking, half the speed, but
the maximum memory requirement drops to around 2300k)."
  (let ((*verbosity* verbosity)
        (*smallp* smallp))
    (decompress-aux in out)))

(defgeneric decompress-aux (in out)
  (:documentation "Specialize behaviour for DECOMPRESS depending on IN and OUT."))

(defmethod decompress-aux ((in stream) (out stream))
  (decompress-stream in out))

(defmethod decompress-aux ((in pathname) out)
  (with-open-file (in-stream in :direction :input :element-type 'octet)
    (decompress-aux in-stream out)))

(defmethod decompress-aux (in (out pathname))
  (with-open-file (out-stream out :direction :output :element-type 'octet :if-exists :supersede)
    (decompress-aux in out-stream)))


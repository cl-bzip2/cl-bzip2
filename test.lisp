;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; test.lisp

;;; Copyright (c) 2008, Chaitanya Gupta.
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;; 1. Redistributions of source code must retain the above copyright
;;;    notice, this list of conditions and the following disclaimer.
;;; 2. Redistributions in binary form must reproduce the above copyright
;;;    notice, this list of conditions and the following disclaimer in the
;;;    documentation and/or other materials provided with the distribution.
;;; 3. The name of the author may not be used to endorse or promote products
;;;    derived from this software without specific prior written permission.
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
;;; IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
;;; OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;; IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
;;; INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
;;; NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
;;; THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package #:cl-bzip2)

(defvar *show-debug-info*)

(defun debug-line (fmt &rest args)
  (when *show-debug-info*
    (apply #'format t (format nil "~&~A~%" fmt) args)
    (force-output *standard-output*)))

(defun test (file-path &key
             (cleanup t)
             (show-debug-info t)
             (compress t)
             (decompress t)
             compare-file-contents
             (block-size-100k *block-size-100k*)
             (verbosity *verbosity*)
             (work-factor *work-factor*)
             (smallp *smallp*))
  "Test compression/decompression.

FILE-PATH is a path designator for the file to be tested. During
compression, FILE-PATH.bz2 will be created. During decompression,
FILE-PATH.new will be created. If CLEANUP is non-NIL,
FILE-PATH.bz2 and FILE-PATH.new will be unconditionally deleted,
regardless of whether they were created during this test or
before.

Setting SHOW-DEBUG-INFO to a non-NIL value sends some debugging
info to *STANDARD-OUTPUT*.

COMPRESS/DECOMRESS control whether compression/decompression is
turned on or off. Both are turned on by default.

COMPARE-FILE-CONTENTS compares the original and decompressed file
byte by byte, otherwise the comparison is just over file length.

For other parameters, see COMPRESS and DECOMPRESS."
  (let* ((*show-debug-info* show-debug-info)
         (test-file-path (namestring file-path))
         (compressed-file-path (format nil "~A.bz2" test-file-path))
         (new-file-path (format nil "~A.new" test-file-path))
         (test-file-length nil)
         (new-file-length nil))
    (macrolet ((with-file ((var file dir) &body body)
                 `(with-open-file (,var
                                   ,file
                                   :direction ,dir
                                   :element-type 'octet
                                   :if-exists :supersede)
                    ,@body)))
      (when compress
        (with-file (in test-file-path :input)
          (setf test-file-length (file-length in))
          (debug-line "Original file size is ~A bytes." test-file-length)
          (debug-line "Compressing...")
          (with-file (out compressed-file-path :output)
            (compress in out
                      :block-size-100k block-size-100k
                      :verbosity verbosity
                      :work-factor work-factor)
            (unless decompress
              (debug-line "Compressed file size is ~A bytes." (file-length out))))))
      (when decompress
        (with-file (in compressed-file-path :input)
          (debug-line "Compressed file size is ~A bytes." (file-length in))
          (debug-line "Decompressing...")
          (with-file (out new-file-path :output)
            (decompress in out
                        :verbosity verbosity
                        :smallp smallp)))
        (with-file (in new-file-path :input)
          (setf new-file-length (file-length in))
          (debug-line "Decompressed file size is ~A bytes." new-file-length))))
    (prog1
        (when (and test-file-length new-file-length)
          (and (= test-file-length new-file-length)
               (if compare-file-contents
                   (progn
                     (debug-line "Comparing file contents...")
                     (file-contents= test-file-path new-file-path))
                   t)))
      (when cleanup
        (and (probe-file compressed-file-path) (delete-file compressed-file-path))
        (and (probe-file new-file-path) (delete-file new-file-path))))))

(defparameter *file-compare-sequence-length* 4096)

(defun file-contents= (file1 file2)
  (with-open-file (in1 file1 :direction :input :element-type 'octet)
    (with-open-file (in2 file2 :direction :input :element-type 'octet)
      (let ((seq1 (make-sequence 'octet-vector *file-compare-sequence-length*))
            (seq2 (make-sequence 'octet-vector *file-compare-sequence-length*)))
        (loop
           for p1 = (read-sequence seq1 in1)
           for p2 = (read-sequence seq2 in2)
           until (and (zerop p1) (zerop p2))
           when (not (and (= p1 p2)
                          (every #'= (subseq seq1 0 p1) (subseq seq2 0 p2))))
           return nil
           finally (return t))))))



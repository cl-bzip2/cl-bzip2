h2. CL-BZIP2 -- BZIP2 compression/decompression for Common Lisp

h3(#introduction). Introduction

cl-bzip2 provides "CFFI":http://common-lisp.net/project/cffi/ bindings for libbzip2 -- the "bzip2":http://www.bzip.org/ compression/decompression library. It comes with a "BSD-style license":http://www.opensource.org/licenses/bsd-license.php.

h3(#contents). Contents

<ol>
  <li><a href="#download">Download and installation</a></li>
  <li><a href="#support">Support and mailing lists</a></li>
  <li><a href="#examples">Examples</a>
    <ol>
      <li><a href="#compression">Compression</a></li>
      <li><a href="#decompression">Decompression</a></li>
      <li><a href="#vectors">Working with vectors</a></li>
    </ol>
  </li>
  <li><a href="#dictionary">The cl-bzip2 dictionary</a>
    <ul>
      <li><a href="#bz_error"><code>bz-error</code></a></li>
      <li><a href="#compress"><code>compress</code></a></li>
      <li><a href="#decompress"><code>decompress</code></a></li>
    </ul>
  </li>
  <li><a href="#limit">Limitations</a></li>
  <li><a href="#acknowledgements">Acknowledgements</a></li>
</ol>

h3(#download). Download and installation

Current release: "Version 0.1.0":http://common-lisp.net/project/cl-bzip2/cl-bzip2-0.1.0.tar.gz

You can also "browse the darcs repository":http://common-lisp.net/cgi-bin/darcsweb/darcsweb.cgi?r=cl-bzip2-cl-bzip2;a=summary or get yourself a copy using darcs get:

<pre>
darcs get http://common-lisp.net/project/cl-bzip2/darcs/cl-bzip2
</pre>

cl-bzip2 has a couple of dependencies:

# "CFFI":http://common-lisp.net/project/cffi/ -- the Common Foreign Function Interface, which provides C bindings for
# "libbzip2":http://www.bzip.org/downloads.html -- The C library for compressing and decompressing data in the bzip2 format. Make sure the shared object library (libbz2.so/libbz2.dylib) is installed on your machine. The bzip2 manual can be found "here":http://www.bzip.org/1.0.5/bzip2-manual-1.0.5.html.

To compile and load cl-bzip2, you can either use ASDF, or simply evaluate @(load (compile-file "bzip2.lisp"))@ while in the cl-bzip2 source directory.

h3(#support). Support and mailing lists

Send questions, bug reports, patches, feature requests, etc. to "cl-bzip2-devel":http://common-lisp.net/cgi-bin/mailman/listinfo/cl-bzip2-devel. Release announcements are made on "cl-bzip2-announce":http://common-lisp.net/cgi-bin/mailman/listinfo/cl-bzip2-announce.

h3(#examples). Examples

h4(#compression). Compression

Use COMPRESS to compress data from a stream/pathname to another stream/pathname. Note that stream should be a binary stream. COMPRESS will not work with string streams i.e. a stream supplied by WITH-INPUT-FROM-STRING will most likely not work at all.

<pre><code>
;;; Compression usage
==<!-- TMPL_VAR remove-me -->==
;;; No values are returned if execution was successful
;;; Using pathnames
CL-USER> (bzip2:compress #p"test.txt" #p"test.txt.bz2")
; No value
==<!-- TMPL_VAR remove-me -->==
;;; Using binary streams
CL-USER> (with-open-file (in "test.txt" :direction :input :element-type '(unsigned-byte 8))
           (with-open-file (out "test.txt.bz2" :direction :output :element-type '(unsigned-byte 8))
             (bzip2:compress in out)))
; No value
==<!-- TMPL_VAR remove-me -->==
;;; Mixing stream and pathname
CL-USER> (with-open-file (in "test.txt" :direction :input :element-type '(unsigned-byte 8))
           (bzip2:compress in #p"test.txt.bz2"))
; No value
</code></pre>

h4(#decompression). Decompression

Use DECOMPRESS to decompress data from a stream/pathname to another stream/pathname. As with COMPRESS, stream should be a binary stream.

<pre><code>
;;; Decompression usage is similar to that for compression
==<!-- TMPL_VAR remove-me -->==
;;; Using pathnames
CL-USER> (bzip2:decompress #p"test.txt.bz2" #p"test.txt")
; No value
==<!-- TMPL_VAR remove-me -->==
;;; Using binary streams
CL-USER> (with-open-file (in "test.txt.bz2" :direction :input :element-type '(unsigned-byte 8))
           (with-open-file (out "test.txt" :direction :output :element-type '(unsigned-byte 8))
             (bzip2:decompress in out)))
; No value
</code></pre>

h4(#vectors). Working with vectors

Compression/decompression of vectors can easily be done with in-memory binary streams by using, for example, "FLEXI-STREAMS":http://weitz.de/flexi-streams/.

<pre><code>
CL-USER> (defvar *vec* #(66 104 97 107 99 104 111 100 105 32 109 97 116 32 107 97 114 111 46))
*VEC*
==<!-- TMPL_VAR remove-me -->==
CL-USER> (flex:with-input-from-sequence (in *vec*)
           (flex:with-output-to-sequence (out)
             (bzip2:compress in out)))
#(66 90 104 57 49 65 89 38 83 89 188 189 88 250 0 0 1 149 128 64 1 16 0
  44 106 148 0 32 0 34 4 245 52 204 144 128 104 3 109 12 42 5 148 84
  110 113 190 46 228 138 112 161 33 121 122 177 244)
==<!-- TMPL_VAR remove-me -->==
CL-USER> (flex:with-input-from-sequence (in *)
           (flex:with-output-to-sequence (out)
             (bzip2:decompress in out)))
#(66 104 97 107 99 104 111 100 105 32 109 97 116 32 107 97 114 111 46)
==<!-- TMPL_VAR remove-me -->==
CL-USER> (equalp * *vec*)
T
</code></pre>

h3(#dictionary). The cl-bzip2 dictionary

p(symbol#bz_error). [Condition type]
<b>bz-error</b>

bq. The default condition type for any BZIP2 compression/decompression related error.

p(symbol#compress). [Function]
<b>compress</b> <i>in out <tt>&amp;key</tt> block-size-100k verbosity work-factor</i>

bq.. Compresses data from <i>@IN@</i> to <i>@OUT@</i>. <i>@IN@</i> or <i>@OUT@</i> can either be a binary stream or a pathname. This function doesn't return any value.

<i>@BLOCK-SIZE-100K@</i> (default 9), <i>@VERBOSITY@</i> (default 0) and <i>@WORK-FACTOR@</i> (default 30) correspond to the parameters <i>@blockSize100k@</i>, <i>@verbosity@</i> and <i>@workFactor@</i>, respectively, for the libbzip2 function <i>@BZ2_bzCompressInit@</i>.

From the bzip2 manual:

Parameter <i>@blockSize100k@</i> specifies the block size to be used for compression. It should be a value between 1 and 9 inclusive, and the actual block size used is 100000 x this figure. 9 gives the best compression but takes most memory.

Parameter <i>@verbosity@</i> should be set to a number between 0 and 4 inclusive. 0 is silent, and greater numbers give increasingly verbose monitoring/debugging output.

Parameter <i>@workFactor@</i> controls how the compression phase behaves when presented with worst case, highly repetitive, input data. If compression runs into difficulties caused by repetitive data, the library switches from the standard sorting algorithm to a fallback algorithm. The fallback is slower than the standard algorithm by perhaps a factor of three, but always behaves reasonably, no matter how bad the input.

p(symbol#decompress). [Function]
<b>decompress</b> <i>in out <tt>&amp;key</tt> verbosity smallp</i>

bq.. Decompresses data from <i>@IN@</i> to <i>@OUT@</i>. <i>@IN@</i> or <i>@OUT@</i> can either be a binary stream or a pathname. This function doesn't return any value.

<i>@VERBOSITY@</i> and <i>@SMALLP@</i> (default NIL) correspond to the parameters <i>@verbosity@</i> and <i>@small@</i>, respectively, for the libbzip2 function <i>@BZ2_bzDecompressInit@</i>.

For the meaning of <i>@VERBOSITY@</i>, see the documentation for COMPRESS. A non-NIL value for <i>@SMALLP@</i> corresponds to a non-zero value for the parameter <i>@small@</i>. Here's what the bzip2 manual says about <i>@small@</i>:

If <i>@small@</i> is nonzero, the library will use an alternative decompression algorithm which uses less memory but at the cost of decompressing more slowly (roughly speaking, half the speed, but the maximum memory requirement drops to around 2300k).

h3(#limit). Limitations

As of now, cl-bzip2 works only with binary streams. I haven't figured out an easy way to make it work with string streams (i.e. easily using them as input). If you know how that can be done, please "let us know":#support.

Also, performance _might_ not be as great as you expect. If you want to improve that, the guts of the code lie in COMPRESS-STREAM and COMPRESS-STREAM-AUX (for compression), and DECOMPRESS-STREAM and DECOMPRESS-STREAM-AUX (for decompression). Any help would be greatly appreciated!

h3(#acknowledgements). Acknowledgements

Thanks to Julian Seward for the bzip2 compression format and the excellent libbzip2 interface.

Thanks also to "Rakesh Pai":http://piecesofrakesh.blogspot.com/ for helping out with the CSS for this page. (And that became my introduction to CSS!). The markup for the cl-bzip2 dictionary was inspired from "DOCUMENTATION-TEMPLATE":http://weitz.de/documentation-template/.
